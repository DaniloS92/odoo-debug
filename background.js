chrome.browserAction.onClicked.addListener(function () {
    debug()
});

chrome.commands.onCommand.addListener(function (command) {
    if (command == 'test') {
        debug()
    }
});

function debug() {
    chrome.tabs.query({
        currentWindow: true,
        active: true
    }, function (tab) {
        var url = tab[0].url
        var el = document.createElement('a');
        el.href = url;
        if (el.search.indexOf('?debug') !== -1) {
            url = el.origin + el.pathname + el.hash;
        } else {
            url = el.origin + el.pathname + '?debug' + el.hash;
        }
        chrome.tabs.update(
            tab[0].id, {
                'url': url
            }
        );
    });
}